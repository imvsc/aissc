package pt.codangel.com.notes;


import pt.codangel.com.feedfacebookdemo.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class NotaBig extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.notabig);
		Bundle t=getIntent().getExtras();
		if(t!=null){
			if(t.containsKey("NOTA")){
				Nota openNota=(Nota) t.get("NOTA");
				Log.d("Nota",openNota.toString());
				TextView textNota=(TextView) findViewById(R.id.notaDetalhes);
				textNota.setText(openNota.toString());
			}
		}
		
		Button backPage=(Button)findViewById(R.id.NotaBig_backButton);
		backPage.setOnClickListener(backpage);
	}
	
	
	OnClickListener backpage=new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			NotaBig.this.finish();
		}
	};
}

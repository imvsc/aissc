package pt.codangel.com.notes;

import pt.codangel.com.feedfacebookdemo.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


public class NewNota extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		 setContentView(R.layout.newnote);
		 
		 ImageView finish=(ImageView)findViewById(R.id.NewNote_BackButton);
		 finish.setOnClickListener(finishActivity);
		 
		 ImageView saveNota=(ImageView)findViewById(R.id.NewNote_AddNote);
		 saveNota.setOnClickListener(saveNotaListener);
	}
	
	OnClickListener saveNotaListener=new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			TextView textNota=(TextView)findViewById(R.id.NewNote_nota);
			Nota notinha=new Nota(textNota.getText().toString());
			
			Intent intent = new Intent();
		    intent.putExtra("NOTA", notinha);
		    setResult(RESULT_OK, intent);
		    finish();
		}
	};
	
	OnClickListener finishActivity=new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Intent intent = new Intent();
		    setResult(RESULT_CANCELED, intent);
		    finish();
			NewNota.this.finish();
		}
	};
	

}

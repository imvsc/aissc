package pt.codangel.com.notes;

import java.util.ArrayList;

import pt.codangel.com.feedfacebookdemo.R;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds.Note;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {
	public ArrayList<Nota> listaDeNotas;
	public ListView lista;
	public NotaAdapter adapterDeNotas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        Button addNota=(Button) findViewById(R.id.Main_Activity_AddNota);
        addNota.setOnClickListener(clickAddNota);
        listaDeNotas=new ArrayList<Nota>();
        
        Nota nova=new Nota("Ola");
        Nota nova2=new Nota("Mundo!");
        
        listaDeNotas.add(nova);
        listaDeNotas.add(nova2);
        
        lista=(ListView)findViewById(R.id.MainActivity_ListaNota);
        
        adapterDeNotas=new NotaAdapter(this, R.layout.notamini, listaDeNotas);
        
        lista.setAdapter(adapterDeNotas);
        lista.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				Nota selectNote=listaDeNotas.get(position);
				Intent t= new Intent(MainActivity.this,NotaBig.class);
				if (selectNote != null) {
					//t.putExtra("NOTA", selectNote.toString());
					t.putExtra("NOTA", selectNote);
				}
				Log.v("Open nota","");
				startActivity(t);
			}
        	
		});
        
        
        lista.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				Nota selectNote=listaDeNotas.get(position);
				Intent t= new Intent(MainActivity.this,NotaBig.class);
				t.putExtra("NOTA", selectNote);
				Log.v("Open nota","");
				startActivityForResult(t, 1);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
        	
		});
	
    }
    
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	 switch (requestCode) {
    	 case 1:{
    		 Bundle res = data.getExtras();
    		 if (resultCode==RESULT_OK) {
    			 if (res.get("NOTA") != null) {
                     Nota result = (Nota) res.get("NOTA");
                     Log.d("FIRST", "result:"+result);
                     listaDeNotas.add(result);
                     adapterDeNotas.notifyDataSetChanged();
    			}
			}
    		
    	
    	 }
    		 break;
    		 
    		default: break;
    	 }
                
        
    }
    
    
    OnClickListener clickAddNota=new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			Intent t= new Intent(MainActivity.this, NewNota.class);
			startActivityForResult(t, 1);
		}
	};


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

package pt.codangel.com.notes;

import java.util.List;

import pt.codangel.com.feedfacebookdemo.R;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class NotaAdapter extends ArrayAdapter<Nota> {
	public Context myContext;
	public int resource;	
	public List<Nota>objects;
	public NotaAdapter(Context context, int resource, List<Nota> objects) {
		super(context, resource, objects);
		myContext=context;
		this.resource=resource;
		this.objects=objects;
		// TODO Auto-generated constructor stub
	}
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView==null){
			LayoutInflater inflater=((Activity)myContext).getLayoutInflater();
			convertView=inflater.inflate(resource, parent,false);
		}
		TextView text=(TextView)convertView.findViewById(R.id.NotaMini_titleNota);
		
		Nota t=objects.get(position);
		text.setText(t.getNota());
		
		return convertView;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return objects.size();
	}
	@Override
	public Nota getItem(int position) {
		// TODO Auto-generated method stub
		return super.getItem(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return super.getItemId(position);
	}

}

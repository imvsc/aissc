package pt.codangel.com.notes;

import java.io.Serializable;

public class Nota implements Serializable {
	public String id;
	public String nota;
	
	public Nota(){
		this.id="";
		this.nota="";
	}
	
	public Nota(String notaNova){
		this.nota=notaNova;
	}
	
	public void setNota(String notaNota){
		this.nota=notaNota;
	}
	
	public String getNota(){
		return this.nota.toString();
	}
	
	public String toString(){
		return this.nota.toString();
	}
	
}
